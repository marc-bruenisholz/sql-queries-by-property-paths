<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths\Responses;

class ErrorResponse extends Response
{
    public Error $error;
}