<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccessControlConditionCreationInterface;
use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccountData;
use ch\_4thewin\SqlSelectModels\Arguments\StringArgument;
use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_IN;

class AccessControlConditionCreation implements AccessControlConditionCreationInterface
{
    protected array $roles;

    function createAccessControlCondition(Table $table): ?ParameterizedSqlInterface
    {
        if(count($this->roles) === 0) {
            throw new \RuntimeException('without roles, no access');
        }
        $arguments = [];
        foreach($this->roles as $role) {
            $arguments[] = new StringArgument($role);
        }
        return new _IN(
            new ColumnExpression($table, 'authorizedRole', 'string'),
            $arguments
        );
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return AccessControlConditionCreation
     */
    public function setRoles(array $roles): AccessControlConditionCreation
    {
        $this->roles = $roles;
        return $this;
    }


    function setAccountData(AccountData $accountData): AccessControlConditionCreationInterface
    {
        // TODO: Use this instead of setRoles
        return $this;
    }
}