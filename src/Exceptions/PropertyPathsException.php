<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths\Exceptions;

use Exception;

class PropertyPathsException extends Exception
{
    protected ?string $location;

    /**
     * @param string $message
     * @param string|null $location
     */
    public function __construct(string $message, ?string $location = null)
    {
        parent::__construct($message);
        $this->location = $location;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }


}