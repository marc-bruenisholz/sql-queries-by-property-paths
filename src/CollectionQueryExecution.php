<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\PropertyPathTreeQueriesBuilder\CollectionQueryNode;
use ch\_4thewin\SqlSelectModels\Arguments\Argument;
use ch\_4thewin\SqlSelectModels\Arguments\BinaryArgument;
use ch\_4thewin\SqlSelectModels\Arguments\StringArgument;
use ch\_4thewin\SqlSelectModels\ParameterizedSql;
use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;
use ch\_4thewin\SqlSelectModels\Select;
use ch\_4thewin\SqppDbConnectorInterface\SqlParameterizedSelectQuery;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_AND;
use ch\_4thewin\SqppDbConnectorInterface\SqlDbConnectorInterface;
use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;
use ch\_4thewin\TreeTraversal\TreeTraversalInterface;
use RuntimeException;

class CollectionQueryExecution implements TreeTraversalInterface
{
    protected array $currentParentIds;
    protected CollectionQueryResultsContainer $collectionQueryResultsContainer;
    protected array $parentIdsStack = [];
    protected SqlDbConnectorInterface $connector;
    protected bool $debugMode = false;

    /**
     * @param array $currentParentIds
     * @param CollectionQueryResultsContainer $collectionQueryResultsContainer
     * @param SqlDbConnectorInterface $connector
     * @param bool $debugMode
     */
    public function __construct(array                           $currentParentIds,
                                CollectionQueryResultsContainer $collectionQueryResultsContainer,
                                SqlDbConnectorInterface         $connector,
                                bool $debugMode)
    {
        $this->currentParentIds = $currentParentIds;
        $this->parentIdsStack[] = $currentParentIds;
        $this->collectionQueryResultsContainer = $collectionQueryResultsContainer;
        $this->connector = $connector;
        $this->debugMode = $debugMode;
    }

    /**
     * @param array $collectionQueryResult
     * @return array The array key is the foreign key and the value is an array containing all rows matching the foreign key
     */
    protected function mapRowsToForeignKeys(array $collectionQueryResult): array {
        $foreignKeyToRowsMapping = [];

        foreach ($collectionQueryResult as $row) {
            // Assuming that the foreign key is always at index 0
            $foreignKey = $row[0];
            if (!isset($foreignKeyToRowsMapping[$foreignKey])) {
                $foreignKeyToRowsMapping[$foreignKey] = [];
            }
            $foreignKeyToRowsMapping[$foreignKey][] = $row;
        }

        return $foreignKeyToRowsMapping;
    }

    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool
    {
        /** @var CollectionQueryNode $node */

        $collectionQuery = $node->getQuery();

        // TODO a bit weird. If paging is used, there is a sub select. Conditions should be added to that sub select
        $fromClause = $collectionQuery->getSelect()->getFromClause();
        $select = null;
        if($fromClause instanceof Select) {
            $select = $fromClause;
        } else {
            $select = $collectionQuery->getSelect();
        }

        $existingWhereCondition = $select->getWhereCondition();

        // In a collection query, the first column is the foreign key containing the parent IDs. A parent id
        //  is a primary key value in the table on the left side of a ToMany relationship.
        // TODO support composite primary keys
        if(count($this->currentParentIds) > 0) {
            // Filter only for relevant rows that can be matched with the left side of the ToMany relationship
            $sqlArguments = [];
            $inExpression = $select->getSelectedColumns()[0]->getColumnExpression()->toString() ;
            if($node->getForeignKeyColumnType() === 'integer') {
                $inExpression = $inExpression . ' IN (' . implode(',', $this->currentParentIds) . ')';
            } elseif($node->getForeignKeyColumnType() === 'string') {
                $inExpression = $inExpression . " IN ('". implode("','", $this->currentParentIds) . "')";
            } elseif($node->getForeignKeyColumnType() === 'binary') {
                $args = [];
                foreach($this->currentParentIds as $currentParentId) {
                    // TODO figure out binary handling
                    $args[] = "UNHEX('".bin2hex($currentParentId)."')";
                }
                $inExpression = $inExpression . " IN (". implode(",", $args) . ")";
            } else {
                throw new RuntimeException('Foreign key column type ' . $node->getForeignKeyColumnType() . ' not supported');
            }
            $parameterizedInExpression = new ParameterizedSql($sqlArguments, $inExpression);

            if ($existingWhereCondition !== null) {
                $newWhereCondition = new _AND($existingWhereCondition, $parameterizedInExpression);
            } else {
                $newWhereCondition = $parameterizedInExpression;
            }
            $select->setWhereCondition($newWhereCondition);
        }


        // Build and run collection query
        // TODO POSSIBLE OPTIMIZATION: DO NOT RUN QUERY IF THERE ARE NO PARENT IDS (WOULD LEAD TO EMPTY IN-EXPRESSION)
        $parameterizedCollectionQuery = $collectionQuery->build();
        $this->connector->prepareStatement($parameterizedCollectionQuery);
        if ($this->debugMode) {
            $this->collectionQueryResultsContainer->addDebugCollectionQuery($this->connector->getSQL());
        }
        $this->connector->executeStatement();
        $collectionQueryResult = [];
        while ($row = $this->connector->fetchRow()) {
            $collectionQueryResult[] = $row;
        }
        $this->connector->closeStatement();

        // Map rows to foreign keys
        $foreignKeyToRowsMapping = $this->mapRowsToForeignKeys($collectionQueryResult);
        $this->collectionQueryResultsContainer->addForeignKeyToRowsMapping($foreignKeyToRowsMapping);

        // Extract primary key ids if needed
        $parentIds = [];
        if(count($node->getSubNodes()) > 0) {
            for($i = 0; $i < count($collectionQueryResult); $i++) {
                // For collection queries, the first column is always the foreign key containing
                // the value matching the primary key of the parent. The second column is the primary key.
                $parentIds[] = $collectionQueryResult[$i][1];
            }
        }

        $this->currentParentIds = $parentIds;
        $this->parentIdsStack[] = $parentIds;

        // aggregations for collection query
        if($node->getAggregationQuery() !== null) {
            // TODO Is IN expression needed? Same as for the normal collection query?
            $parameterizedAggregationQuery = $node->getAggregationQuery()->build();
            $this->connector->prepareStatement($parameterizedAggregationQuery);
            $this->connector->executeStatement();
            $foreignKeyToAggregationRowMapping = [];
            while (($aggregationRow = $this->connector->fetchRow()) !== null) {
                // TODO support composite keys
                $foreignKeyToAggregationRowMapping[(string)$aggregationRow[0]] = $aggregationRow;
            }
            $this->collectionQueryResultsContainer->addForeignKeyToAggregationRowMapping($foreignKeyToAggregationRowMapping);
            $this->connector->closeStatement();
        }


        $this->collectionQueryResultsContainer->addCollectionQueryResult($collectionQueryResult);
        return true;
    }

    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void
    {
        array_pop($this->parentIdsStack);
        $this->currentParentIds = $this->parentIdsStack[count($this->parentIdsStack)-1];
    }
}