<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths\Responses;

class SuccessResponse extends Response
{
    public array $data;
}