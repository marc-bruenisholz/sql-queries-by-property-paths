<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths\Responses;

use JsonSerializable;

class Error implements JsonSerializable
{
    public string $cause;

    public ?string $location = null;

    public function jsonSerialize(): array
    {
        $object = [];
        $object['cause'] = $this->cause;
        if($this->location !== null) {
            $object['location'] = $this->location;
        }
        return $object;
    }
}