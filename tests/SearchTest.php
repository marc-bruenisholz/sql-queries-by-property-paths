<?php /** @noinspection SqlNoDataSourceInspection */

namespace ch\_4thewin\SqlQueriesByPropertyPathsTest;

use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccountData;
use ch\_4thewin\SqlQueriesByPropertyPaths\AccountDataExtractorInterface;
use ch\_4thewin\SqlQueriesByPropertyPaths\RequestHandler;
use ch\_4thewin\SqppTestData\DummyDoctrinePropertyPermissions;
use ch\_4thewin\SqppTestData\DummyORMDataProvider;
use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\Utils;
use PHPUnit\Framework\TestCase;

use Psr\Http\Message\ServerRequestInterface;
use function PHPUnit\Framework\assertEquals;

class SearchTest extends TestCase
{
    protected static TestDB $testDb;

    public static function setUpBeforeClass(): void
    {
        self::$testDb = new TestDB('test.db');
        self::$testDb->init();
    }

    public function testPageAndSortCombinedOnProperty()
    {
        $requestBody = [
            'query' => [
                'Person' => [
                    '$meta' => [
                        'sort' => [
                            ['name', 'DESC']
                        ],
                    ],
                    'rivals' => [
                        '$meta' => [
                            'sort' => [
                                ['name', 'ASC']
                            ],
                            'page' => [
                                'offset' => 0,
                                'limit' => 1
                            ]
                        ],
                        'name' => []
                    ],
                    'name' => []
                ]
            ]
        ];
        $request = (new ServerRequest('POST', '/execute-query'))
            ->withHeader('Content-Type', 'application/json')
            ->withBody(Utils::streamFor(json_encode($requestBody)));

        $accountDataExtractor = new class implements AccountDataExtractorInterface {
            function extractAccountData(ServerRequestInterface $request): AccountData
            {
                return new AccountData(['user', 'admin'], 'userId');
            }
        };

        $propertyPathsRequestHandler = new RequestHandler(
            self::$testDb,
            new DummyORMDataProvider(),
            'id',
            $accountDataExtractor,
            new DummyDoctrinePropertyPermissions()
        );
        $httpResponse = $propertyPathsRequestHandler->handle($request, true);

        $parsedResponseBody = json_decode($httpResponse->getBody(), true);

        assertEquals(
        // Note that person id is needed to match rows from the collection query.
            "SELECT `person`.`id`,`person`.`name` FROM `person` WHERE `person`.`authorizedRole` ".
            "IN ('user','admin') ORDER BY `person`.`name` DESC",
            $parsedResponseBody['baseQuery']
        );

        assertEquals(
            "SELECT * FROM (SELECT `person_person_rivals`.`person1_id`,`rivals`.`name`,".
            "row_number() OVER (PARTITION BY `person_person_rivals`.`person1_id`) as __rowNumber ".
            "FROM `person_person_rivals` JOIN `person` `rivals` ON `rivals`.`id` = `person_person_rivals`.`person2_id` ".
            "AND `rivals`.`authorizedRole` IN ('user','admin') WHERE (`rivals`.`authorizedRole` IN ('user','admin') ".
            "AND `person_person_rivals`.`person1_id` IN ('3','2','1')) ORDER BY `rivals`.`name`) `person_person_rivals` ".
            "WHERE (`person_person_rivals`.`__rowNumber` > 0 AND `person_person_rivals`.`__rowNumber` < 2)",
            $parsedResponseBody['collectionQueries'][0]
        );

        assertEquals([
            'Person' => [
                [
                    "name" => "Peter",
                    'rivals' => [
                        [
                            'name' => 'Hans'
                        ]
                    ]
                ],

                [
                    "name" => "Max",
                    'rivals' => [

                    ]
                ],
                [
                    "name" => "Hans",
                    'rivals' => [
                        [
                            'name' => 'Peter'
                        ]
                    ]
                ]
            ]
        ], $parsedResponseBody['data']);
    }

    public function testSortOnProperty()
    {
        $requestBody = [
            'query' => [
                'Person' => [
                    'rivals' => [
                        '$meta' => [
                            'sort' => [
                                ['name', 'ASC']
                            ]
                        ],
                        'name' => []
                    ],
                    'name' => []
                ]
            ]
        ];
        $request = (new ServerRequest('POST', '/execute-query'))
            ->withHeader('Content-Type', 'application/json')
            ->withBody(Utils::streamFor(json_encode($requestBody)));

        $accountDataExtractor = new class implements AccountDataExtractorInterface {
            function extractAccountData(ServerRequestInterface $request): AccountData
            {
                return new AccountData(['user'], 'userId');
            }
        };

        $propertyPathsRequestHandler = new RequestHandler(
            self::$testDb,
            new DummyORMDataProvider(),
            'id',
            $accountDataExtractor,            new DummyDoctrinePropertyPermissions()

        );
        $httpResponse = $propertyPathsRequestHandler->handle($request, true);

        $parsedResponseBody = json_decode($httpResponse->getBody(), true);

        assertEquals(
        // Note that person id is needed to match rows from the collection query.
            "SELECT `person`.`id`,`person`.`name` FROM `person` WHERE `person`.`authorizedRole` IN ('user')",
            $parsedResponseBody['baseQuery']
        );

        assertEquals(
            "SELECT `person_person_rivals`.`person1_id`,`rivals`.`name` ".
            "FROM `person_person_rivals` JOIN `person` `rivals` ON `rivals`.`id` = `person_person_rivals`.`person2_id` ".
            "AND `rivals`.`authorizedRole` IN ('user') WHERE (`rivals`.`authorizedRole` IN ('user') ".
            "AND `person_person_rivals`.`person1_id` IN ('1','2','3')) ORDER BY `rivals`.`name`",
            $parsedResponseBody['collectionQueries'][0]
        );
    }

    public function testSortOnRoot()
    {
        $requestBody = [
            'query' => [
                'Person' => [
                    '$meta' => [
                        'sort' => [
                            ['name', 'ASC']
                        ]
                    ],
                    'name' => []
                ]
            ]
        ];
        $request = (new ServerRequest('POST', '/execute-query'))
            ->withHeader('Content-Type', 'application/json')
            ->withBody(Utils::streamFor(json_encode($requestBody)));

        $accountDataExtractor = new class implements AccountDataExtractorInterface {
            function extractAccountData(ServerRequestInterface $request): AccountData
            {
                return new AccountData(['user'], 'userId');
            }
        };

        $propertyPathsRequestHandler = new RequestHandler(
            self::$testDb,
            new DummyORMDataProvider(),
            'id',
            $accountDataExtractor    ,        new DummyDoctrinePropertyPermissions()

        );
        $httpResponse = $propertyPathsRequestHandler->handle($request, true);

        $parsedResponseBody = json_decode($httpResponse->getBody(), true);

        assertEquals(
            "SELECT `person`.`name` FROM `person` WHERE `person`.`authorizedRole` IN ('user') ORDER BY `person`.`name`",
            $parsedResponseBody['baseQuery']
        );
    }

    public function testManyToManySearch()
    {
        // Give me all the people that have the person with name Peter as a friend.
        $requestBody = [
            'query' => [
                'Person' => [
                    'friends' => [
                        '$meta' => [
                            'filter' => [
                                'expression' => [
                                    'name' => [
                                        'eq' => 'Peter'
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'name' => []
                ]
            ]
        ];

        $request = (new ServerRequest('POST', '/execute-query'))
            ->withHeader('Content-Type', 'application/json')
            ->withBody(Utils::streamFor(json_encode($requestBody)));

        $accountDataExtractor = new class implements AccountDataExtractorInterface {

            function extractAccountData(ServerRequestInterface $request): AccountData
            {
                return new AccountData(['user'], 'userId');
            }
        };

        $propertyPathsRequestHandler = new RequestHandler(
            self::$testDb,
            new DummyORMDataProvider(),
            'id',
            $accountDataExtractor,            new DummyDoctrinePropertyPermissions()

        );

        $httpResponse = $propertyPathsRequestHandler->handle($request, true);

        $parsedResponseBody = json_decode($httpResponse->getBody(), true);

        assertEquals(
        // TODO id column is needed to match rows from ToMany relationships. But if no field is rendered
        //  from ToMany relationship no id is needed.
            "SELECT DISTINCT `person`.`id`,`person`.`name` FROM `person` ".
            "JOIN `person_person_friends` ON `person`.`id` = `person_person_friends`.`person1_id` ".
            "JOIN `person` `friends` ON `friends`.`id` = `person_person_friends`.`person2_id` ".
            "AND (`friends`.`authorizedRole` IN ('user') AND `friends`.`name` = 'Peter') WHERE `person`.`authorizedRole` IN ('user')",
            $parsedResponseBody['baseQuery']
        );

        assertEquals(0, count($parsedResponseBody['collectionQueries']));

        assertEquals([
            'Person' => [
                [
                    "name" => "Hans"
                ]
            ]
        ], $parsedResponseBody['data']);

        // Hans is the only person that has a person with name 'Peter' as friend.
    }
}