<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\DoctrinePropertyPermissionAnnotation\DoctrinePropertyPermissionsInterface;
use ch\_4thewin\ORMDataProviderInterface\ORMDataProviderInterface;
use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccessControlConditionCreationInterface;
use ch\_4thewin\SqlQueriesByPropertyPaths\Exceptions\JsonDecodeException;
use ch\_4thewin\SqlQueriesByPropertyPaths\Exceptions\PropertyPathsException;
use ch\_4thewin\SqlQueriesByPropertyPaths\Exceptions\ValidationException;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\Error;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\ErrorResponse;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\Response;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\UsageErrorResponse;
use ch\_4thewin\SqppAddOrmDataToQuery\exceptions\MappingException;
use ch\_4thewin\SqppAddOrmDataToQuery\SqppAddOrmDataToQuery;
use ch\_4thewin\SqppDbConnectorInterface\SqlDbConnectorInterface;
use ch\_4thewin\SqppTypedQuery\limitations\LimitationConfig;
use ch\_4thewin\SqppTypedQuery\ObjectToTypedQueryConverter;
use ch\_4thewin\SqppTypedQuery\PropertyPathToTreeConversion;
use ch\_4thewin\TreeTraversal\TreeTraversalExecution;
use Exception;
use GuzzleHttp\Psr7\Utils;
use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
use League\OpenAPIValidation\PSR7\ServerRequestValidator;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use League\OpenAPIValidation\Schema\Exception\SchemaMismatch;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;


class RequestHandler
{
    /** @var SqlDbConnectorInterface */
    protected SqlDbConnectorInterface $connector;

    /** @var ORMDataProviderInterface */
    protected ORMDataProviderInterface $ormDataProvider;

    /** @var string */
    protected string $idPropertyName;

    /** @var ServerRequestValidator */
    protected ServerRequestValidator $openapiValidator;

    protected AccountDataExtractorInterface $accountDataExtractor;

    protected QueryExecution $queryExecution;
    protected TreeTraversalExecution $treeTraversalExecution;
    protected PropertyPathToTreeConversion $propertyPathToTreeConversion;
    protected SqppAddOrmDataToQuery $sqppAddOrmDataToQuery;
    protected DoctrinePropertyPermissionsInterface $doctrinePropertyPermissions;
    protected AccessControlConditionCreationInterface $accessControlConditionCreation;
    protected LimitationConfig $limitationConfig;

    /**
     * @param SqlDbConnectorInterface $connector
     * @param ORMDataProviderInterface $ormDataProvider
     * @param string $idPropertyName
     * @param AccountDataExtractorInterface $accountDataExtractor
     * @param DoctrinePropertyPermissionsInterface $doctrinePropertyPermissions
     * @param AccessControlConditionCreationInterface|null $accessControlConditionCreation
     * @param LimitationConfig|null $limitationConfig
     */
    public function __construct(SqlDbConnectorInterface $connector, ORMDataProviderInterface $ormDataProvider,
                                string $idPropertyName, AccountDataExtractorInterface $accountDataExtractor,
                                DoctrinePropertyPermissionsInterface $doctrinePropertyPermissions,
                                AccessControlConditionCreationInterface $accessControlConditionCreation = null,
                                LimitationConfig $limitationConfig = null
    )
    {
        $this->connector = $connector;
        $this->ormDataProvider = $ormDataProvider;
        $this->idPropertyName = $idPropertyName;
        $this->accountDataExtractor = $accountDataExtractor;
        $this->doctrinePropertyPermissions = $doctrinePropertyPermissions;

        $yamlFile = __DIR__ . "/openapidefinition.yaml";
        $this->openapiValidator = (new ValidatorBuilder())->fromYamlFile($yamlFile)->getServerRequestValidator();

        if($accessControlConditionCreation === null) {
            $this->accessControlConditionCreation = new AccessControlConditionCreation();
        } else {
            $this->accessControlConditionCreation = new $accessControlConditionCreation;
        }

        if($limitationConfig === null) {
            $this->limitationConfig = new LimitationConfig(
            // TODO arbitrary limits. What would be reasonable values?
                100,
                100,
                100
            );
        } else {
            $this->limitationConfig = $limitationConfig;
        }

        $this->queryExecution = new QueryExecution($ormDataProvider, $idPropertyName,
            $connector, $this->accessControlConditionCreation);

        $this->treeTraversalExecution = new TreeTraversalExecution();
        $this->sqppAddOrmDataToQuery = new SqppAddOrmDataToQuery(
            $this->ormDataProvider,
            $this->treeTraversalExecution,
            new PropertyPathToTreeConversion()
        );
    }

    /**
     * @param ServerRequestInterface $request
     * @param bool $debugMode
     * @return Response
     * @throws JsonDecodeException
     * @throws ValidationException
     * @throws Exception
     */
    protected function handleHttpRequest(ServerRequestInterface $request, bool $debugMode = false): Response
    {
        // Get roles and give them to the entity/property based access control engine
        $accountData = $this->accountDataExtractor->extractAccountData($request);
        $roles = $accountData->getRoles();
        // TODO improve design. roles are set here but effect is somewhere far away (in DoctrineORMDataProvider).
        $this->doctrinePropertyPermissions->setRoles($roles);
        // TODO setRoles was relaced with setAccountData
        $this->accessControlConditionCreation->setRoles($roles);
        $this->accessControlConditionCreation->setAccountData($accountData);

        // Validation
        try {
            $this->openapiValidator->validate($request);
        } catch (ValidationFailed $e) {
            $previous = $e->getPrevious();
//            $message = null;
            $location = null;
            if ($previous instanceof SchemaMismatch) {
                $message = $previous->getMessage();
                $location = implode('.', $previous->dataBreadCrumb()->buildChain());
            } else {
                $message = $e->getMessage();
            }

            // Fix misleading message
            if($message === "Value expected to be 'array', 'array' given.") {
                $message = "Value expected to be 'array', 'object' given.";
            }
            throw new ValidationException($message, $location);
        }

        // decode JSON body
        $bodyString = (string)$request->getBody();
        $decodedJson = json_decode($bodyString, true);
        // TODO set limits (json depth, body size)
        if ($decodedJson === null) {
            throw new JsonDecodeException('Could not decode JSON body.');
        }

        // Convert data structure into typed object hierarchy
        $objectToTypedQueryConverter = new ObjectToTypedQueryConverter(
            $this->limitationConfig
        );
        $typedQuery = $objectToTypedQueryConverter->convert($decodedJson);

        // Add ORM Data to query
        try {
            $this->sqppAddOrmDataToQuery->addOrmDataToQuery($typedQuery);
        } catch(MappingException $e) {
            throw new PropertyPathsException($e->getMessage(), $e->getLocation());
        }

        return $this->queryExecution->executeQuery($typedQuery, $debugMode);
    }

    protected function exceptionHandlingLayer(ServerRequestInterface $request, bool $debugMode = false): Response {
        try {
            return $this->handleHttpRequest($request, $debugMode);
        }
        catch(PropertyPathsException $e) {
            $error = new Error();
            $error->cause = $e->getMessage();
            $error->location = $e->getLocation();
            $errorResponse = new UsageErrorResponse();
            $errorResponse->error = $error;
            return $errorResponse;
        }
        // all other exceptions. E.g. caused by implementation errors.
        catch (Throwable $e) {
            $error = new Error();
            if($debugMode) {
                $error->cause = $e->getMessage() . "\n" . $e->getTraceAsString();
            } else {
                // Hide errors in production
                $error->cause = 'An undisclosed error occurred.';
            }
            $errorResponse = new ErrorResponse();
            $errorResponse->error = $error;
            return $errorResponse;
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param bool $debugMode
     * @param bool $prettyPrint
     * @return \GuzzleHttp\Psr7\Response
     */
    public function handle(ServerRequestInterface $request, bool $debugMode = false, bool $prettyPrint = false): \GuzzleHttp\Psr7\Response
    {

        $applicationResponse = $this->exceptionHandlingLayer($request, $debugMode);

        $httpResponse = new \GuzzleHttp\Psr7\Response();

        if($applicationResponse instanceof UsageErrorResponse) {
            $httpResponse = $httpResponse->withStatus(400);
        } elseif($applicationResponse instanceof ErrorResponse) {
            $httpResponse = $httpResponse->withStatus(500);
        } else {
            $httpResponse = $httpResponse->withStatus(200);
        }

        return $httpResponse
            ->withHeader('Content-Type', 'application/json')
            ->withBody(Utils::streamFor(json_encode($applicationResponse, $prettyPrint ? JSON_PRETTY_PRINT : 0)));
    }
}