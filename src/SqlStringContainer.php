<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\SqlSelectModels\ParameterizedSqlInterface;

class SqlStringContainer implements ParameterizedSqlInterface
{
    protected string $string;

    /**
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }


    function getArguments(): array
    {
       return [];
    }

    function toString(): string
    {
        return $this->string;
    }
}