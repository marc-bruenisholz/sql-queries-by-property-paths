<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;


class CollectionQueryResultsContainer
{
    /**
     * @var array[]
     */
    protected array $collectionQueryResults = [];

    protected array $debugCollectionQueries = [];

    protected array $foreignKeyToRowsMappings = [];
    
    protected array $foreignKeyToAggregationRowMapping = [];

    /**
     * @return array[]
     */
    public function getCollectionQueryResults(): array
    {
        return $this->collectionQueryResults;
    }

    /**
     * @param array[] $collectionQueryResult
     * @return void
     */
    public function addCollectionQueryResult(array $collectionQueryResult): void {
        $this->collectionQueryResults[] = $collectionQueryResult;
    }

    /**
     * @return array
     */
    public function getDebugCollectionQueries(): array
    {
        return $this->debugCollectionQueries;
    }

    public function addDebugCollectionQuery(string $collectionQuery): void {
        $this->debugCollectionQueries[] = $collectionQuery;
    }

    public function addForeignKeyToRowsMapping(array $foreignKeyToRowsMapping): void {
        $this->foreignKeyToRowsMappings[] = $foreignKeyToRowsMapping;
    }

    /**
     * @return array
     */
    public function getForeignKeyToRowsMappings(): array
    {
        return $this->foreignKeyToRowsMappings;
    }

    public function addForeignKeyToAggregationRowMapping(array $foreignKeyToAggregationRowMapping): void {
        $this->foreignKeyToAggregationRowMapping[] = $foreignKeyToAggregationRowMapping;
    }

    /**
     * @return array
     */
    public function getForeignKeyToAggregationRowMapping(): array
    {
        return $this->foreignKeyToAggregationRowMapping;
    }
    
    
}