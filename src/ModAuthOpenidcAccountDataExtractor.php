<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccountData;
use Psr\Http\Message\ServerRequestInterface;

class ModAuthOpenidcAccountDataExtractor implements AccountDataExtractorInterface
{
    function extractAccountData(ServerRequestInterface $request): AccountData
    {
        $rolesHeaders = $request->getHeader('OIDC_CLAIM_roles');
        if(count($rolesHeaders) > 0) {
            $roles = explode(',', $request->getHeader('OIDC_CLAIM_roles')[0]);
        } else {
            $roles = [];
        }
        //        $userId = $request->getHeader('OIDC_CLAIM_sub')[0];
        $userId = $request->getHeader('OIDC_CLAIM_preferred_username')[0];
        return new AccountData($roles, $userId);
    }
}