<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths\Responses;

class DebugSuccessResponse extends SuccessResponse
{
    public string $baseQuery;

    public string $aggregateBaseQuery;

    /** @var string[] */
    public array $collectionQueries;
}