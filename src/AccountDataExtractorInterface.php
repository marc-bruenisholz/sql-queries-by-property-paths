<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccountData;
use Psr\Http\Message\ServerRequestInterface;

interface AccountDataExtractorInterface
{
    function extractAccountData(ServerRequestInterface $request): AccountData;
}