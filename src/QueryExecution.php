<?php

namespace ch\_4thewin\SqlQueriesByPropertyPaths;

use ch\_4thewin\ORMDataProviderInterface\ORMDataProviderInterface;
use ch\_4thewin\PropertyPathQueriesResultToObjectConverter\PropertyPathRowsToObjectConverter;
use ch\_4thewin\PropertyPathTreeQueriesBuilder\AccessControlConditionCreationInterface;
use ch\_4thewin\PropertyPathTreeQueriesBuilder\PropertyPathTreeQueriesBuilder;
use ch\_4thewin\SqlQueriesByPropertyPaths\Exceptions\PropertyUnavailableException;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\DebugSuccessResponse;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\Error;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\Response;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\SuccessResponse;
use ch\_4thewin\SqlQueriesByPropertyPaths\Responses\UsageErrorResponse;
use ch\_4thewin\SqlSelectModels\Arguments\StringArgument;
use ch\_4thewin\SqppDbConnectorInterface\SqlDbConnectorInterface;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_IN;
use ch\_4thewin\SqppTypedQueryModels\Query;
use ch\_4thewin\SqppTypedQueryToPropertyPathTreeConversion\TypedQueryRootToPropertyPathTreeConversion;
use ch\_4thewin\SqppTypedQueryToPropertyPathTreeConversion\TypeMismatchException;
use ch\_4thewin\TreeTraversal\TreeTraversal;

class QueryExecution
{
    protected ORMDataProviderInterface $ormDataProvider;
    protected string $idPropertyName;
    protected SqlDbConnectorInterface $connector;

    protected string $debugBaseQuery;

    protected string $debugAggregateBaseQuery;

    /** @var string[] */
    protected array $debugCollectionQueries;

    protected AccessControlConditionCreationInterface $accessControlConditionCreation;

    /**
     * @param ORMDataProviderInterface $ormDataProvider
     * @param string $idPropertyName
     * @param SqlDbConnectorInterface $connector
     * @param AccessControlConditionCreationInterface $accessControlConditionCreation
     */
    public function __construct(ORMDataProviderInterface $ormDataProvider, string $idPropertyName,
                                SqlDbConnectorInterface $connector,
                                AccessControlConditionCreationInterface $accessControlConditionCreation)
    {
        $this->ormDataProvider = $ormDataProvider;
        $this->idPropertyName = $idPropertyName;
        $this->connector = $connector;
        $this->accessControlConditionCreation = $accessControlConditionCreation;
    }

    protected function exceptionHandlingLayer(Query $query, bool $debugMode = false): Response
    {
        try {
            $data = $this->internalHandling($query, $debugMode);

            if ($debugMode) {
                $debugSuccessResponse = new DebugSuccessResponse();
                $debugSuccessResponse->data = $data;
                $debugSuccessResponse->baseQuery = $this->debugBaseQuery;
                $debugSuccessResponse->aggregateBaseQuery = $this->debugAggregateBaseQuery;
                $debugSuccessResponse->collectionQueries = $this->debugCollectionQueries;
                return $debugSuccessResponse;
            } else {
                $successResponse = new SuccessResponse();
                $successResponse->data = $data;
                return $successResponse;
            }
        } // exceptions caused by misuse
        catch (PropertyUnavailableException $e) {
            $error = new Error();
            $error->cause = $e->getMessage();
            $error->location = $e->getLocation();
            $errorResponse = new UsageErrorResponse();
            $errorResponse->error = $error;
            return $errorResponse;
        }
    }

    public function executeQuery(Query $query, bool $debugMode = false): Response
    {
        return $this->exceptionHandlingLayer($query, $debugMode);
    }


    /**
     * @throws PropertyUnavailableException
     */
    protected function internalHandling(Query $query, bool $debugMode = false): array
    {
        if ($debugMode) {
            $this->debugBaseQuery = '';
            $this->debugCollectionQueries = [];
            $this->debugAggregateBaseQuery = '';
        }

        // Prepare converters
        $typedQueryRootToPropertyPathTreeConversion = new TypedQueryRootToPropertyPathTreeConversion($this->idPropertyName);

        // Collect results from each root
        $data = [];
        foreach ($query->getRoots() as $root) {
            $rootName = $root->getName();

            try {
                $rootPropertyPathNode = $typedQueryRootToPropertyPathTreeConversion->convert($root);
            } catch(TypeMismatchException /*| ORMMappingException*/ $e) {
                // TODO specify which exception are made public and which are internal (undisclosed)
                throw new PropertyUnavailableException($e->getMessage());
            }

            // Generate SQL Queries from tree
            $propertyPathTreeQueriesBuilder = new PropertyPathTreeQueriesBuilder($this->accessControlConditionCreation);
            // TODO strictly speaking, the root node is not a "collection query node" but a "base query node"
            //  consider using different types for root and anything not root. Same concept already applied to
            //  property path nodes. See RootPropertyPathNode or RelationshipPropertyPathNode etc.
            $collectionQueryNode = $propertyPathTreeQueriesBuilder->build($rootPropertyPathNode);


            // BASE QUERY ------------------------
            // Execute Base query first and extract primary key values (IDs)
            $parameterizedBaseQuery = $collectionQueryNode->getQuery()->build();
            $this->connector->openReadonly();
            $this->connector->prepareStatement($parameterizedBaseQuery);
            if ($debugMode) {
                $this->debugBaseQuery = $this->connector->getSQL();
            }
            $this->connector->executeStatement();
            $baseRows = [];
            while (($baseRow = $this->connector->fetchRow()) !== null) {
                $baseRows[] = $baseRow;
            }
            $this->connector->closeStatement();
            // Extract root ids to constrain output of collection queries
            $rootIdNode = $rootPropertyPathNode->getIdNode();
            $rootIds = [];
            if ($rootIdNode !== null && $rootIdNode->isNeededForToMany()) {
                // TODO support composite primary keys (need to save column count)
                for ($i = 0; $i < count($baseRows); $i++) {
                    // ID column is always at index 0
                    $rootIds[] = $baseRows[$i][0];
                }
            }
            // Base query aggregations
            if($collectionQueryNode->getAggregationQuery() !== null) {
                $parameterizedBaseAggregationQuery = $collectionQueryNode->getAggregationQuery()->build();
                $this->connector->prepareStatement($parameterizedBaseAggregationQuery);
                if ($debugMode) {
                    $this->debugAggregateBaseQuery = $this->connector->getSQL();
                }
                $this->connector->executeStatement();
                $aggregationRow = $this->connector->fetchRow();
                $this->connector->closeStatement();
            }



            // COLLECTION QUERIES ------------------
            $collectionQueryResultsContainer = new CollectionQueryResultsContainer();
            $collectionQueryTreeTraversal = new TreeTraversal(
                new CollectionQueryExecution($rootIds, $collectionQueryResultsContainer, $this->connector, $debugMode));
            $collectionQueryTreeTraversal->traverse($collectionQueryNode);
            $this->debugCollectionQueries = $collectionQueryResultsContainer->getDebugCollectionQueries();

            // Convert baseRows with rows from collection queries into objects.
            $propertyPathQueriesResultToObjectConverter = new PropertyPathRowsToObjectConverter(
                $rootPropertyPathNode,
                $collectionQueryResultsContainer->getForeignKeyToRowsMappings(),
                $collectionQueryResultsContainer->getForeignKeyToAggregationRowMapping()
            );

            // convert to objects
            $objects = [];
            for ($i = 0; $i < count($baseRows); $i++) {
                $objects[] = $propertyPathQueriesResultToObjectConverter->convert($baseRows[$i]);
            }
            $data[$rootName] = $objects;

            // add aggregation for root
            // TODO code duplication (see property-path-queries-result-to-object-converter
            $rootMeta = $root->getRootMeta();
            if($rootMeta !== null) {
                $aggregates = $rootMeta->getAggregates();
                if(count($aggregates) > 0) {
                    $aggregationObject = [];
                    $aggregateColumnIndex = 0;
                    foreach($aggregates as $aggregate) {
                        $propertyName = $aggregate->getPropertyName();
                        $aggregationObject[$propertyName] = [];
                        $functions  = $aggregate->getFunctions();
                        foreach ($functions as $function) {
                            $aggregationObject[$propertyName][$function] = $aggregationRow[$aggregateColumnIndex++];
                        }
                    }
                    $data[$rootName . '_aggregate'] = $aggregationObject;
                }
            }

        }

        return $data;
    }
}