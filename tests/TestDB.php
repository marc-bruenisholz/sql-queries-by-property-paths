<?php
/** @noinspection SqlResolve */

/** @noinspection SqlNoDataSourceInspection */

namespace ch\_4thewin\SqlQueriesByPropertyPathsTest;


use ch\_4thewin\SqppSqliteDbConnector\SQLiteDBConnector;

class TestDB extends SQLiteDBConnector
{
    public function init() {
        $this->open('test.db');
        $this->exec('DROP TABLE IF EXISTS person');
        $this->exec('DROP TABLE IF EXISTS address');
        $this->exec('DROP TABLE IF EXISTS person_person_friends');
        $this->exec('DROP TABLE IF EXISTS person_person_rivals');
        $this->exec('CREATE TABLE person (id TEXT PRIMARY KEY, name TEXT, addressId TEXT, bestFriendId TEXT, authorizedRole TEXT)');
        $this->exec('CREATE TABLE address (id TEXT PRIMARY KEY, street TEXT, authorizedRole TEXT)');
        $this->exec('CREATE TABLE person_person_friends (id TEXT PRIMARY KEY, person1_id TEXT, person2_id TEXT)');
        $this->exec('CREATE TABLE person_person_rivals (id TEXT PRIMARY KEY, person1_id TEXT, person2_id TEXT)');
        $this->exec("INSERT INTO person VALUES ('1', 'Hans', '1', '2', 'user')");
        $this->exec("INSERT INTO person VALUES ('2', 'Max', '1', null, 'user')");
        $this->exec("INSERT INTO person VALUES ('3', 'Peter', '1', null, 'user')");
        // TODO It is unclear how ORMs usually handle ManyToMany relationships. Do they save both directions or only one and infere the other direction?
        $this->exec("INSERT INTO person_person_friends VALUES ('1', '1', '2')");
        $this->exec("INSERT INTO person_person_friends VALUES ('2', '2', '1')");
        $this->exec("INSERT INTO person_person_friends VALUES ('3', '1', '3')");
        $this->exec("INSERT INTO person_person_rivals VALUES ('3', '1', '3')");
        $this->exec("INSERT INTO person_person_rivals VALUES ('4', '3', '1')");
        $this->exec("INSERT INTO address VALUES ('1', 'Sesam Street', 'user')");
        $this->closeDb();
    }
}